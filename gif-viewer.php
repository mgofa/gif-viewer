<?php
/**
 * Plugin Name: GIF Viewer
 * Plugin URI: http://www.imge.com
 * Description: Intercepts GIF uploads and wraps them in some JS to prevent automatic loading on mobile devices. 
 * Version: 1.0
 * Author: Chris Lewis
 * Author URI: http://www.imge.com
 * License: Free non-commercial use
 */

// Assets

function load_scripts() {
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'gif-viewer-script', plugins_url( 'gif-viewer.js', __FILE__ ), array( 'jquery' ), '', true );
  wp_enqueue_style( 'gif-viewier-styles', plugins_url( 'gif-viewer.css', __FILE__ ) );
}
add_action( 'wp_enqueue_scripts', 'load_scripts' );

// Shortcode

function generate_wrapper( $attributes, $content = null ) {
	$play_button_url = plugins_url( 'play_button.png', __FILE__ );
	$a = shortcode_atts( array( "url" => "", "width" => "", "background" => "", "ar" => "100" ), $attributes );
	$class = str_replace( "=", "", base64_encode( $a[ "ar" ] ) );
	$template = <<<HTML
<style type="text/css">
.%s:before {
	padding-top: %s%%;
	content: "";
	display: block;
}
</style>
<div class="gif_wrapper gifhidden %s" style="width: %s; background-image: url( %s );" image="%s">
	<div class="overlay" style="background-image: url( %s );"></div>
</div>
HTML;
	return sprintf( $template, $class, $a[ "ar" ] * 100, $class, $a[ "width" ], $a[ "background" ], $a[ "url" ], $play_button_url );
}
add_shortcode( "gif_wrapper", "generate_wrapper" );

// Hook into media insert

function imge_media_insert( $html, $id, $attachment ) {
	$attachment = get_post( $id );
	$mime_type = $attachment->post_mime_type;
	if( $mime_type == "image/gif" ) {
		$data = wp_get_attachment_image_src( $id, '', true );
		$url = $data[ 0 ];
		$width = $data[ 1 ];
		$height = $data[ 2 ];

		// use 'hack' to get thumbnail
		$thumbnail_data = wp_get_attachment_image_src( $id, array( 300, 149 ), false );
		return sprintf( "[gif_wrapper url=%s width='100%%' ar=%s background=%s]", $url, $height / $width, $thumbnail_data[ 0 ] );
	} else {
		return $html;
	}
}
add_filter( "media_send_to_editor", "imge_media_insert", 25, 3 );

?>
