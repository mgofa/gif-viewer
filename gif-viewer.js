jQuery = jQuery || $;

// http://stackoverflow.com/questions/11381673/javascript-solution-to-detect-mobile-browser/13819253#13819253
var isMobile = {
    Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
    BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
    iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
    Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
    Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
        },
    any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
};

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

function reveal( x ) {
  if( x.hasClass( 'gifhidden' ) ) {
    x.css( "background-image", "url(" + x.attr( "image" ) + ")" ).removeClass( "gifhidden" ).find( ".overlay" ).remove();
  }
}

jQuery( document ).ready( function() {
  if( isMobile.any() ) {
    jQuery( ".gif_wrapper.gifhidden" ).click( function( event ) {
      reveal( jQuery( this ) );
      event.preventDefault();
    } )
  } else {
    jQuery( ".gif_wrapper.gifhidden" ).each( function() { reveal( jQuery( this ) ) } );;
  }
} );
